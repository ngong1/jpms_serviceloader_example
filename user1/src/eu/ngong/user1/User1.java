package eu.ngong.user1;

import eu.ngong.myService.MyService;

public class User1 implements MyService {

	@Override
	public String name() {
		return "User1";
	}

	@Override
	public void doSomething() {
		System.out.println("User1 is acting.");
	}

}
