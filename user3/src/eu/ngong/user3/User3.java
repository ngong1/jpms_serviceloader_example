package eu.ngong.user3;

import eu.ngong.myService.MyService;

public class User3 implements MyService {

	@Override
	public void doSomething() {
		System.out.println("User3 is acting");
	}

	@Override
	public String name() {
		return "User3";
	}

}
