package eu.ngong.user2;

import eu.ngong.myService.MyService;

public class User2 implements MyService {

	@Override
	public String name() {
		return "User2";
	}

	@Override
	public void doSomething() {
		System.out.println("User2 is acting");
	}

}
