package eu.ngong.myService;

import java.util.ServiceLoader;

public class ServiceUser implements MyService {
	private static MyService myService = new ServiceUser();

	public static void main(String[] args) {
		if (args.length > 0) {
			System.out.println("trying to load " + args[0] + " envirionment.");
			ServiceLoader.load(MyService.class).forEach(s -> {
				System.out.println(s.name());
				if (s.name().equalsIgnoreCase(args[0])) {
					myService = s;
				}
			});
		}
		myService.doSomething();
	}

	@Override
	public void doSomething() {
		System.out.println("The default service is acting.");
	}

	@Override
	public String name() {
		return "Default";
	}
}
