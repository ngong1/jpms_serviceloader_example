# JPMS_ServiceLoader_Example

## Description
Provides a Java desktop program with a default service, that can be overloaded by the individual user. A user provides their own service class and give their name as an argument on the command line.

## Installation
Download the 4 Java projects myService, user1, user2, user3. Compile each to a seperate jar file. Tests were done using Java-11.

## Usage
### intended usage
assume this directory structure:
* JPMS_ServiceLoader_Example
	* myService
	* user1

Put the jar files inside, e.g. , e.g. myService/myService.jar, user1/user1.jar

cd into myService
`java -p ..\user1\user1.jar;myService.jar -m MyService/eu.ngong.myService.ServiceUser User1`

this should output

>trying to load User1 envirionment.<BR/>
>User1<BR/>
>User1 is acting

### interesting usage for investigation

assume this directory structure:
* JPMS_ServiceLoader_Example
	* myService
	* user1
	* user2
	* user3
	* repository
 
and put all .jar files in the repository.

`java -p repository -m MyService/eu.ngong.myService.ServiceUser User3`

leads to 
> trying to load User3 envirionment.<BR/>
> User2<BR/>
> User1<BR/>
> User3<BR/>
> User3 is acting

You may investigate further with
`java --show-module-resolution -p repository -m MyService/eu.ngong.myService.ServiceUser User3`

## License
GPL Version 3
